(defcustom unity-scheme-command "mono Client.exe"
  "Command for unity scheme")

(defun unity-scheme ()
  (interactive)
  (let ((default-directory "/home/rolando/Downloads/ischeme/IronScheme"))
	(run-lisp unity-scheme-command)))
