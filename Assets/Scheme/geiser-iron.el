(defun geiser-iron--binary ()
  "returns mono"
  "mono")

(defun geiser-iron--parameters ()
  '("/home/rolando/Documents/unity/TestScheme/Repl.exe"))

(defconst geiser-iron--prompt-regexp "> ")

(defun geiser-iron--geiser-procedure (proc &rest args)
  nil)

(defun geiser-iron--exit-command () "(exit 0)")

(defconst geiser-iron-minimum-version "0.1")

(defun geiser-iron--version (binary)
  (car (process-lines binary "/home/rolando/Documents/unity/TestScheme/Repl.exe" "-v")))

(define-geiser-implementation iron
  (binary geiser-iron--binary)
  (arglist geiser-iron--parameters)
  (version-command geiser-iron--version)
  (minimum-version geiser-iron-minimum-version)
  ;; (repl-startup geiser-iron--startup)
  (prompt-regexp geiser-iron--prompt-regexp)
  (debugger-prompt-regexp nil) ;; geiser-iron--debugger-prompt-regexp
  ;; (enter-debugger geiser-iron--enter-debugger)
  (marshall-procedure geiser-iron--geiser-procedure)
  ;; (find-module geiser-iron--get-module)
  ;; (enter-command geiser-iron--enter-command)
  (exit-command geiser-iron--exit-command)
  ;; (import-command geiser-iron--import-command)
  ;; (find-symbol-begin geiser-iron--symbol-begin)
  ;; (display-error geiser-iron--display-error)
  ;; (external-help geiser-iron--manual-look-up)
  ;; (check-buffer geiser-iron--guess)
  ;; (keywords geiser-iron--keywords)
  ;; (case-sensitive geiser-iron-case-sensitive-p)
  )


(geiser-impl--add-to-alist 'regexp "\\.ss$" 'iron t)
(geiser-impl--add-to-alist 'regexp "\\.sls$" 'iron t)

(provide 'geiser-iron)
