(import (ironscheme clr))
(clr-using UnityEngine)
(clr-using UnityScheme)

(define (create-primitive type)
  (clr-static-call UnityScheme.Util CreatePrimitive type))

(define (new-vec3 vec)
  (clr-new Vec3 (vector-ref vec 0) (vector-ref vec 1) (vector-ref vec 2)))

(define (set-position go position)
  (let* ((pos (new-vec3 position)))
	(clr-prop-set! Transform position (clr-prop-get Transform transform go) pos)))

(let
	(create-primitive 3))
