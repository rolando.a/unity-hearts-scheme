﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IronScheme.Runtime;

public class SchemeBehaviour : MonoBehaviour {

	private List<Callable> _actions;

	public void AddHook(Callable c)
	{
		_actions.Add(c);
	}

	// Use this for initialization
	void Awake () {
		_actions = new List<Callable>();
	}

	void Start() {
	}
	
	// Update is called once per frame
	void Update () {
		foreach (var item in _actions)
		{
			item.Call(this.gameObject);
		}
	}
}
