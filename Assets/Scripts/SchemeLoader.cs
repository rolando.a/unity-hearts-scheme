﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

// udp server
using System.Net;
using System.Net.Sockets;
using System.Text;

using UnityEngine;

namespace UnityScheme
{

using IronScheme;
using IronScheme.Runtime;
using IronScheme.Remoting.Server;

public struct Message {
	public string msg;
	public IPEndPoint endPoint;
}
public class SchemeLoader : MonoBehaviour {

	private UdpClient socket;
	private Queue<Message> evalQueue;

	private static void OnUdpData(System.IAsyncResult result) {
		string receivedData;
		SchemeLoader loader = result.AsyncState as SchemeLoader;
		UdpClient socket = loader.socket;
		IPEndPoint source = new IPEndPoint(0, 0);
		byte[] message = socket.EndReceive(result, ref source);
		//Debug.LogFormat("client: {0} {1}", source.Address, source.Port);
		receivedData = Encoding.ASCII.GetString(message, 0, message.Length);
		//Debug.LogFormat("received: {0}", receivedData);
		lock (loader)
		{
			var msg = new Message() {
				msg = receivedData,
				endPoint = source
			};
			loader.evalQueue.Enqueue(msg);
		}

		socket.BeginReceive(new System.AsyncCallback(OnUdpData), loader);
	}

	private void StartServer() {
		socket = new UdpClient(11000);
		socket.BeginReceive(new System.AsyncCallback(OnUdpData), this);
		//yield return null;
	}

	private object EvalString(string str, out bool result) {
		result = true;
		try {
			return str.Eval();
		} catch(System.Exception err) {
			Debug.LogError(err);
			result = false;
		}
		return "Error";
	}

	// Use this for initialization
	void Start () {
		//Callable load = "load".Eval<Callable>();
		//load.Call("/home/rolando/Documents/unity/TestScheme/Assets/IronScheme/test2.scm");

		//var started = Host.Start();
		//Debug.LogFormat("SchemeServer started: {0}", started);
		evalQueue = new Queue<Message>();
		StartServer();
		//StartCoroutine("StartServer");
	}

	// Update is called once per frame
	void Update () {
		while (evalQueue.Count > 0) {
			var msg = evalQueue.Dequeue();
			bool evalResult;
			var resp = EvalString(msg.msg, out evalResult);
			var outBytes = Encoding.ASCII.GetBytes(resp.ToString());
			//Debug.LogFormat("eval: {0}", resp);
			socket.Send(outBytes, outBytes.Length, msg.endPoint);
		}
	}
}
	
}
