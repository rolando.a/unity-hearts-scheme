using UnityEngine;

namespace UnityScheme
{
public class Util {
	public static GameObject CreatePrimitive(int type) {
		return GameObject.CreatePrimitive((PrimitiveType)type);
	}
}

}