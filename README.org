* What's this?

Trying to get scheme running on Unity, to make Unity games. This is
highly experimental and almost nothing is functional yet.

So far, this is just a fun hack/quick demo.

* Why?

Why not. This work is heavily inspired by the awesome [[https://github.com/arcadia-unity/Arcadia][Arcadia]].

* Requisites

- Unity 2017.3 (should work on Unity 2017.1 and prob older versions as
  well)
- Mono (or Windows)

* How to run

- Open the Unity Project
- Make sure the player is set to Run in the Background:
  [[./Images/run-in-background.png]]
- Hit play in the Editor
- Connect the Repl:
  ~mono Repl.exe~

* How to use geiser on emacs

(need to update this section)

- Install [[https://github.com/jaor/geiser][geiser]]
- Eval geiser-iron.el file on Emacs (make sure to change the path to
  the ~Repl.exe~ binary)
- Open an scheme file (you can try Assets/Scheme/test2.ss)
- M-x run-iron
- Play with the repl (you can copy/paste code from the scheme buffer
  into the repl, eval from the scheme buffer is not ready yet).
