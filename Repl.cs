using System;
using System.Text.RegularExpressions;
// udp server
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Text;


// UDP client code from:
// https://stackoverflow.com/a/19787486/9161762
public struct Received
{
    public IPEndPoint Sender;
    public string Message;
}

abstract class UdpBase
{
    protected UdpClient Client;

    protected UdpBase()
    {
        Client = new UdpClient();
    }

	public Received ReceiveSync()
	{
		IPEndPoint remote = new IPEndPoint(0, 0);
		var result = Client.Receive(ref remote);
		return new Received()
		{
			Message = Encoding.ASCII.GetString(result, 0, result.Length),
            Sender = remote
		};
	}

    public async Task<Received> Receive()
    {
        var result = await Client.ReceiveAsync();
        return new Received()
        {
            Message = Encoding.ASCII.GetString(result.Buffer, 0, result.Buffer.Length),
            Sender = result.RemoteEndPoint
        };
    }
}

//Client
class UdpUser : UdpBase
{
    private UdpUser(){}

    public static UdpUser ConnectTo(string hostname, int port)
    {
        var connection = new UdpUser();
        connection.Client.Connect(hostname, port);
        return connection;
    }

    public void Send(string message)
    {
        var datagram = Encoding.ASCII.GetBytes(message);
        Client.Send(datagram, datagram.Length);
    }

}

class Program {
	// from https://github.com/arcadia-unity/Arcadia/blob/develop/Editor/repl-client.rb#L21
	static bool IsBalanced(string rr) {
		var rx1 = new Regex(@"[^(){}\[\]]");
		var rx2 = new Regex(@"\(\)|\[\]|\{\}");
		rr = rx1.Replace(rr, "");
		while (rx2.Match(rr).Success) {
			rr = rx2.Replace(rr, "");
		}
		return rr.Length == 0;
	}
	
	static void Main(string[] args) {
		var str = "";
        bool notBalanced = false;
		var client = UdpUser.ConnectTo("127.0.0.1", 11000);

		if (args.Length == 1 && args[0] == "-v") {
			Console.WriteLine("0.1");
			return;
		}

		while (true) {
            Console.Write((notBalanced ? "> " : "iron> "));
			var nl = Console.ReadLine();
			str = String.Concat(str, nl);
            if (str.Length > 0) {
                if (IsBalanced(str)) {
					client.Send(str);
					var received = client.ReceiveSync();
    				Console.WriteLine("~> " + received.Message);
	    			str = "";
                    notBalanced = false;
                } else {
    				str = String.Concat(str, "\n");
                    notBalanced = true;
                }
            }
		}
	}
}
